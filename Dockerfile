FROM openjdk:8
MAINTAINER Sruti Misra (sruti.misra@infostretch.com)
RUN apt-get update &&\
    apt-get install -y maven
COPY pom.xml /usr/local/service/pom.xml
COPY src /usr/local/service/src
WORKDIR /usr/local/service
RUN mvn package
CMD ["java","-cp","target/myapp-0.1.0.jar","com.infostretch.myapp.MyAppApplication"]
Expose 8080
